<?php
use Illuminate\Database\Seeder;

use App\Models\User;
use Faker\Factory as Faker;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::truncate();//Empty all user record

        $faker = Faker::create();

        $password = Hash::make('somepassword');

        $i = 0;
        $provider = 'social';
        $provider_id = '12345';
        while ($i < 50) {
            $start_date = '1980-12-31 00:00:00';
            $end_date = '1994-01-01 00:00:00';

            $min = strtotime($start_date);
            $max = strtotime($end_date);

            // Generate random number using above bounds
            $val = rand($min, $max);
            $weeks = rand(1, 52);

            // Convert back to desired date format
            $dob = new DateTime(date('Y-m-d H:i:s', $val));
            $current_date = date('Y-m-d H:i:s');
            $gender = $faker->randomElement(['non-binary', 'male', 'female']);
            User::create([
                'first_name' => $faker->FirstName,
                'last_name' => $faker->LastName,
                'email' => $faker->unique()->email,
                'password' => $password,
                'provider' => $provider,
                'provider_id' => $provider_id,
                'verified' => 1,
                'dob' => $dob,
                'gender' => $gender,
                'verification_token' => str_random(30),
                'created_at' => $current_date,
            ]);
            $i++;
        }

        User::create([
            'first_name' => 'Adminstration',
            'last_name' => 'Rules',
            'email' => 'amin@test.com',
            'password' => $password,
            'provider' => $provider,
            'provider_id' => $provider_id,
            'dob' => '1987-02-14',
            'gender' => 'male',
            'verified' => 1,
            'verification_token' => str_random(30),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
