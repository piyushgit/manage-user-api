<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('provider');
            $table->string('provider_id');
            $table->integer('status')->default(1);
            $table->date('dob');
            $table->enum('gender', ['non-binary', 'male', 'female']);
            $table->rememberToken();
            $table->timestamp('created_at')->nullable();
            $table->tinyInteger('verified')->default(0);
            $table->string('verification_token')->unique()->nullable();
            $table->timestamp('last_login_time')->nullable();
            $table->string('last_login_ip')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
