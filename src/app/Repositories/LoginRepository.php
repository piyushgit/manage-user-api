<?php
namespace App\Repositories;

use App\Models\User;
use App\Models\UserAudit;

class LoginRepository
{
    public function getUser($email)
    {
        return User::Where('email', $email)->first();
    }

    public function create(array $data)
    {
        $user = new User();

        $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);

        $user->fill($data);
        $user->save();

        return $user;
    }
    public function setLogoutAudit($id)
    {
        $userAudit = new UserAudit();
        $userAudit->setAttribute('user_id', $id);
        $userAudit->setAttribute('activity', 'Logout');
        $userAudit->setAttribute('comments', '');
        return $userAudit;
    }
    public function setLoginAudit($id)
    {
        $userAudit = new UserAudit();
        $userAudit->setAttribute('user_id', $id);
        $userAudit->setAttribute('activity', 'Login');
        $userAudit->setAttribute('comments', '');
        return $userAudit;
    }
}
