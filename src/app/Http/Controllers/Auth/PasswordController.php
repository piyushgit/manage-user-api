<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\PasswordService;
use App\Services\UserService;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use JWTAuth;
use JWTAuthException;
use Illuminate\Support\Facades\Log;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
     */
    private $passwordService, $userService;
    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PasswordService $passwordService, UserService $userService)
    {
        $this->passwordService = $passwordService;
        $this->userService = $userService;
       // $this->middleware('guest');
    }

    public function resetTokenVerify($token)
    {
        //retrieve user's email by PasswordResetToken
        $user = $this->passwordService->getEmailByPasswordResetToken($token);
        if (count($user) > 0) {
            $response = $this->passwordService->verifyPasswordRecovery($token, $user->email);
            return response()->json($response, $response['status']);
        }
        return response()->json(['error' => 'Token Invalid!'], 401);
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(Request $request)
    {
        $response = $this->passwordService->resetPassword($request->all());

        return response()->json($response, $response['status']);

    }

    /**
     * Change Password for Loggedin user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        $id = $user->id;
        //dd($id);
        $response = $this->passwordService->changePassword($request->all(), $id);
        return response()->json($response, $response['status']);
    }

    public function getAuthenticatedUser()
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

	// the token is valid and we have found the user via the sub claim
        return $user;
    }

    /**
     * Send resetPasswordLink to user email-address
     * API parmaters [email]
     * API response 
     * send reset Password Token Link to [email] 
     * @param  \Illuminate\Http\Request  $request
     * @return response
     */
    public function requestResetPassword(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);
        $enteredEmail = $request->input('email');
        $user = $this->userService->getUserByEmail($enteredEmail);
        if ($user) {
            $token = $this->passwordService->getForgotPasswordToken($user);
            if (!is_null($token)) {
                //Send email to user to reset password
                $user->sendPasswordResetNotification($token);
            }
            else{
                Log::error("Should have got the token from the passsowrd service requested foir : " . $enteredEmail);
            }
        }
        else            {
            Log::warning("Forgot password request with unknown email " . $enteredEmail);
        }
        return response()->json(['operation' => 'requestResetPassword', 'response' => 'success', 'message' => 'Reset Password Mail Sent Successfully'], 200);
    }
}
