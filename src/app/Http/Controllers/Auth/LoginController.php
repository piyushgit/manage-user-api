<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Services\LoginService;
use Illuminate\Http\Request;

use JWTAuth;
use JWTAuthException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Auth;
use Socialite;
use Carbon\Carbon;
use App\Events\Login;
use App\Events\LogAudit;

class LoginController extends Controller
{
    
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'http://localhost:3000';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $loginService;

    public $successStatus = 200;

    public function __construct(LoginService $loginService)
    {
        //$this->middleware('guest')->except('logout');
        $this->loginService = $loginService;
       /* $this->middleware('jwt.refresh', [
            'only' => [
                'refreshToken'
            ]
        ]);
         */
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        if (Auth::check()) {
            $user = Auth::user();
            return $user;
        }
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Check User Login Attempt with JWTAuth
     *
     * @return Response
     */
    public function handleLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $token = null;
        try {
            if (!$token = $this->guard()->attempt($credentials)) {
                return $this->sendFailedLoginResponse($request);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['failed_to_create_token'], 500);
        }
        return $this->sendLoginResponse($request, $token);
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $userExists = $this->loginService->findOrCreateUser($user, $provider);

        Auth::login($userExists, true);
    
        // Login and "remember" the given user...
        return redirect($this->redirectTo); 
        //return Redirect::to($url);   
    }

    /**
     * Log out and update LogAudit event for user
     * @return Response
     */
    public function handleLogout(Request $request)
    {
        $token = '';
        if ($request->has('token')) {
            $token = $request->token;
            try {
            /*
            $tokenFetch = JWTAuth::parseToken()->authenticate();
            if ($tokenFetch) {
                $token = str_replace("Bearer ", "", $request->header('Authorization'));
            }
            JWTAuth::setToken($token);
                 */
                JWTAuth::parseToken()->authenticate();
                JWTAuth::setToken($token);
                $user = JWTAuth::toUser();

            } catch (JWTException $e) {
                if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                    return response()->json(['error' => 'token_expired'], 422);
                } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                    return response()->json(['error' => 'token_invalid'], 401);
                } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenBlacklistedException) {
                    return response()->json(['error' => 'Token Blacklisted!'], 500);
                } else {
                    return response()->json(['error' => 'Token is required'], 404);
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            // Set Attribute Here for UserAudit
            $userAudit = $this->loginService->setLogoutAudit($user->id);                 
            //Unset the token here
            JWTAuth::setToken($token)->invalidate();
        } else {
            Auth::logout();
            //return redirect($this->redirectTo);
        }

        event(new LogAudit($userAudit));

        return response()->json([
            'message' => \Lang::get('Logout!'),
        ], $this->successStatus);
    }
    /**
     * SendLoginResponse if get any Json webtoken 
     * @return success response
     */
    protected function sendLoginResponse(Request $request, string $token)
    {
        $this->clearLoginAttempts($request);
        return $this->authenticated($request, $this->guard()->user(), $token);
    }
    /**
     * Checking the authenticated user 
     * if user exists update in user Logaudit ,Login events
     * also checking remember request if true set the token expiry
     * default expiry of token is 1 hour //check 'ttl'=>60 in config/jwt.php 
     */
    protected function authenticated(Request $request, $user, string $token)
    {

        if ($user->verified == 1) {
            $userAudit = $this->loginService->setLoginAudit($user->id);
            event(new Login($user));
            event(new LogAudit($userAudit));
            $customClaims = array();
            if ($request->has('remember') && $request->remember == true) {
                $expiration = Carbon::now()->addWeeks(1)->getTimestamp();
                $customClaims = ['exp' => $expiration];
            }
            if (count($customClaims) > 0) {
                $credentials = $request->only('email', 'password');
                $token = $this->guard()->attempt($credentials, $customClaims);
            }
            return response()->json([
                'operation' => 'login_request',
                'success' => compact('token')
            ], $this->successStatus);
        }
        return response()->json([
            'operation' => 'login_request',
            'response' => 'login failed',
            'message' => 'non-verified user! Please confirm your email to login!'
        ], 422);
    }
    /**
     * Failed login due to invalid credentials 
     * @return Response
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return response()->json([
            'message' => \Lang::get('auth.failed'),
        ], 401);
    }
}
