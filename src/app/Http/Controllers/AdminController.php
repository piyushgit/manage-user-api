<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Services\AdminService;
use App\Http\Requests\ResendSignupEmailRequest;

class AdminController extends ApiController
{

    private $adminService;
    public function __construct(AdminService $adminService)
    {
        $this->adminService = $adminService;
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Activate Deactivate the user
     *
     * @param  string verificationToken
     * @return \Illuminate\Http\Response
     */
    public function activateDeactivateUser($verificationToken)
    {
        $response = $this->adminService->activateDeactivateUser($verificationToken);

        return response()->json($response, $response['status']);
    }

    /**
     * Resend User Signup email
     * @param registered email
     *  @return response
     * 
     */
    public function resendSignUpEmail(ResendSignupEmailRequest $request)
    {        
        //get verification token by email
        $enteredEmail = $request['email'];
        $user = $this->adminService->getUserByEmail($enteredEmail);
        if ($user) {
            $this->adminService->sendRegistrationEmail($user);
            return response()->json(['operation' => 'resendSignUpEmail', 'response' => 'success', 'message' => 'Resend Signup Mail Sent Successfully'], 200);

        } else {
            \Log::warning("Resend Email request with unknown email " . $enteredEmail);
            return response()->json(['operation' => 'resendSignUpEmail', 'response' => 'failed', 'message' => 'unknown email'], 401);
        }


    }

}
