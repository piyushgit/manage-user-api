<?php
namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\ConfirmRegisterUserRequest;

use Illuminate\Support\Facades\Log;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Jobs\UserAccountCreatedNotice;

/**
 * Class DashboardController
 *
 * @package App\Http\Controllers
 */

// Actions Handled By Resource Controller
// Verb    URI                        Action    Route Name
// GET    /users                    index    users.index
// GET    /users/create            create    users.create
// POST    /users                  store    users.store
// GET    /users/{photo}            show    users.show
// GET    /users/{photo}/edit        edit    users.edit
// PUT/PATCH    /users/{photo}    update    users.update
// DELETE    /users/{photo}        destroy    users.destroy

class UserController extends ApiController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/api/user",
     *     description="Returns user overview.",
     *     operationId="api.user.index",
     *     produces={"application/json"},
     *     tags={"user"},
     *     @SWG\Response(
     *         response=200,
     *         description="User overview."
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     */

    private $userService;
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Creates a new user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createUser(CreateUserRequest $request)
    {
        //print_r($request->all());
        $response = $this->userService->create($request->all());

        return response()->json($response, $response['status']);
    }

    /**
     * 
     * Update the user profile 
     * @param  \Illuminate\Http\Request  $request
     * @param  JWT TOKEN
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(UpdateUserRequest $request)
    {
        
        // Edit user profile
        $user = ($this->getAuthUser($request))->getData();
        $id = $user->id;
        $response = $this->userService->update($request->all(), $id);
        return response()->json($response, $response['status']);

    }

    /**
     * Authorized user information from db
     *
     *@param encrypted json web token
     *@return User Information
     */

    public function getAuthUser(Request $request)
    {
        try {
            $token = JWTAuth::getToken();
            if (false === $token) {
                return response()->json(['error' => 'token_invalid'], 401);
            }

            $user = JWTAuth::toUser();

        } catch (JWTException $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json(['error' => 'token_expired'], 422);
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json(['error' => 'token_invalid'], 401);
            } else {
                return response()->json(['error' => 'Token is required'], 404);
            }
        }
        return response()->json($user);

    }
    /**
     * User registration verification link confirmation
     * @param encrypted verificationtoken
     *  @return comfirmation message for valid link and user
     * 
     */
    public function confirmUser(ConfirmRegisterUserRequest $request)
    {
        $verificationToken = $request['verificationToken'];
        $response = $this->userService->confirmRegisteredUser($verificationToken);
        return response()->json($response, $response['status']);
    }


}
