<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Request;

class ConfirmRegisterUserRequest extends FormRequest
{
    public function __construct()
    {

        return Request::all();
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'verificationToken' => 'required|string|min:2',
        ];
    }
    public function messages()
    {
        return [
            'verificationToken.required' => 'verificationToken is required',
        ];
    }
}
