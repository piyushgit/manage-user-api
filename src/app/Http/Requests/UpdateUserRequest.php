<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required|string|min:2',
            'lastName' => 'required|string|min:2',
            'gender' => 'required|in:1,2,3',
            'dob' => 'required|date_format:Y-m-d|before:tomorrow',
            'token' => 'required|string|min:2'
        ];

    }

    public function messages()
    {
        return [
            ' firstName . required ' => ' firstName is required ',
            ' lastName . required ' => ' lastName is required ',
            ' gender . required ' => ' Gender field is required - Input values between 1 to 3 ',
            ' dob . required ' => ' Date of Birth is required',
            ' token . required' => ' Valid JWT Token Is required'
        ];
    }
}
