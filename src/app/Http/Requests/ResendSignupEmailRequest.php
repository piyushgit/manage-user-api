<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Request;

class ResendSignupEmailRequest extends FormRequest
{
    public function __construct()
    {
        return Request::all();
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'email' => 'required|email',

        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'An email address is required'
        ];
    }
}
