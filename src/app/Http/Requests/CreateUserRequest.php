<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Request;

class CreateUserRequest extends FormRequest
{
    public function __construct()
    {
        return Request::all();
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'firstName' => 'required|string|min:2',
            'lastName' => 'required|string|min:2',
            'email' => 'required|email',
            'passwordConfirm' => 'required|string|min:6',
            'gender' => 'required|in:1,2,3',
            'dob' => 'required|date_format:Y-m-d|before:tomorrow',
        ];
    }

    public function messages()
    {
        return [
            'firstName.required' => 'firstName is required',
            'lastName.required' => 'lastName is required',
            'email.required' => 'An email attribute is required',
            'passwordConfirm.required' => 'Password Confirm is required',
            'gender.required' => 'Gender field is required-Input values between 1 to 3',
            'dob.required' => 'Date of Birth is required'
        ];
    }
}
