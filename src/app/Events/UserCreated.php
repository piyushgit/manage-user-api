<?php

namespace Api\Events;

use Infrastructure\Events\Event;
use App\Models\User;

class UserWasCreated extends Event
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
}