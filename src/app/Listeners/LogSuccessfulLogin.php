<?php
namespace App\Listeners;

use Carbon\Carbon;
use App\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSuccessfulLogin implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        // Update user last login date/time

        $event->user->update(['last_login_time' => Carbon::now(), 'last_login_ip' => \Request::ip()]);
    }
}
