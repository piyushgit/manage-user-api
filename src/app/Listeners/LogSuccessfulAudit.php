<?php
namespace App\Listeners;

use Carbon\Carbon;

use App\Events\LogAudit;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSuccessfulAudit
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LogAudit  $event (Login/Logout)
     * @return void
     */
    public function handle(LogAudit $event)
    {
        \App\Models\UserAudit::create([
            'user_id' => $event->userAudit->user_id,
            'activity' => $event->userAudit->activity,
            'activity_time' => Carbon::now(),
            'ip_address' => \Request::ip(),
            'comments' => $event->userAudit->comments,
        ]);

    }
}
