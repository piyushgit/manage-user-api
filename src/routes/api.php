<?php
use Illuminate\Http\Request;
 
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::apiResource('users', 'userController');

Route::group(['prefix' => 'users', 'middleware' => ['cors', \App\Http\Middleware\Cors::class]], function () {
    Route::post('/', 'UserController@createUser');
    /**
     * Resend User Signup email
     * @param  request email-address 
     * @return response
     */
    Route::post('resend/email', 'AdminController@resendSignupEmail');

    Route::post('login', 'Auth\LoginController@handleLogin')->middleware('web');
    Route::post('logout', 'Auth\LoginController@handleLogout');

    /**
     * Verify registered user's email link
     * @param  request verificationToken
     * @return response 
     */
    Route::post('confirm', 'UserController@confirmUser');

    /**
     * Request to reset the password by passing email as parameter
     * @return token
     *
     */
    Route::post('password/request', 'Auth\PasswordController@requestResetPassword');

    /**
     * resetPasswordLink Verification process when user recieved reset Password Email
     *  @parameter:token
     * @return response
     *
     */
    Route::get('password/verifytoken/{token}', 'Auth\PasswordController@resetTokenVerify');

    /**
     * Reset User's Password After resetLink Verification success
     *  @parameter:email, passwordConfirm,token
     * @return response
     *
     */
    Route::post('password/reset', 'Auth\PasswordController@resetPassword');


    /**
     * get User details by passing access token as a input
     * @return response
     */
    Route::get('me/profile', 'UserController@getAuthUser');

    /**
     * Edit User Profile
     * @param  request
     * @param  jwt authenicated token
     * @return response
     */
    Route::post('me/profile', 'UserController@updateProfile');

    /**
     * Change Password for user
     *  @parameter: oldPassword,passwordConfirm
     * @return response
     *
     */
    Route::post('me/password', 'Auth\PasswordController@changePassword');

    //Social providers login route
    Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
    Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
});



 