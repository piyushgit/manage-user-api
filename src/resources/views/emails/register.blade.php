<!DOCTYPE html>
<html lang="en-US">

<head>
	<meta charset="utf-8">
</head>

<body>
	<h4>Dear {!! $user->first_name !!} {!! $user->last_name !!},</h4>

	<h2> Welcome and thanks for registering on {{ config('app.name') }} .</h2>

	<div>
		Please click
		<a href="{{ URL::to(env('APP_WEB_URL').'/register/verify:'. $user->verification_token) }}">here</a> to activate your account.After activation you can start using the portal.
		<br/>
	</div>
	<div>
		We are sure you will enjoy using this service.
		<br/> Please don't hesitate to send us your feedback or comments.
	</div>
	<div>
		<br/> If the above link does not work,please copy and paste the link below on your browser's address bar
		<br/>

		<a href="{{ URL::to(env('APP_WEB_URL').'/register/verify:'. $user->verification_token) }}">{{ URL::to(env('APP_WEB_URL').'/register/verify:'. $user->verification_token) }}</a>

		<br/>
		<br/>
	</div>
	<div>We wish you a pleasant experience using our portal.
		<br/>Regards
	</div>

</body>

</html>